ifneq (,$(wildcard ./.env))
    include .env
    export
endif

cmd-exists-%:
	@hash $(*) > /dev/null 2>&1 || \
		(echo "ERROR: '$(*)' must be installed and available on your PATH."; exit 1)
dbup: cmd-exists-docker
	docker run -d --name realworld-db -p "${DB_PORT}":5432 -e POSTGRES_PASSWORD="${DB_PASS}" -e POSTGRES_USER="${DB_USER}" -d postgres:13

dbstart: cmd-exists-docker
	docker start realworld-db

dbstop: cmd-exists-docker
	docker stop realworld-db
	docker rm realworld-db

dbcreate: cmd-exists-docker
	docker exec -it realworld-db createdb --username="${DB_USER}" --owner="${DB_USER}" "${DB_NAME}"

dbdown: cmd-exists-docker
	docker exec -it realworld-db dropdb --username="${DB_USER}" "${DB_NAME}"

dbconn: cmd-exists-psql
	psql -U "${DB_USER}" -h "${DB_HOST}" -p "${DB_PORT}" "${DB_NAME}"

migrateup: cmd-exists-migrate
	migrate -path db/migration -database "postgresql://"${DB_USER}":"${DB_PASS}"@"${DB_HOST}":"${DB_PORT}"/"${DB_NAME}"?sslmode=disable" -verbose up

migrateup1: cmd-exists-migrate
	migrate -path db/migration -database "postgresql://"${DB_USER}":"${DB_PASS}"@"${DB_HOST}":"${DB_PORT}"/"${DB_NAME}"?sslmode=disable" -verbose up 1

migratedown: cmd-exists-migrate
	migrate -path db/migration -database "postgresql://"${DB_USER}":"${DB_PASS}"@"${DB_HOST}":"${DB_PORT}"/"${DB_NAME}"?sslmode=disable" -verbose down

migratedown1: cmd-exists-migrate
	migrate -path db/migration -database "postgresql://"${DB_USER}":"${DB_PASS}"@"${DB_HOST}":"${DB_PORT}"/"${DB_NAME}"?sslmode=disable" -verbose down 1

sqlc: cmd-exists-sqlc
	sqlc generate

appstart: cmd-exists-go
	go run main.go

.PHONY: dbup dbstart dbstop dbcreate dbdown migrateup migrateup1 migratedown migratedown1 sqlc dbconn appstart
