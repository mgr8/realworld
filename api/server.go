package api

import (
	"github.com/gin-gonic/gin"
	db "gitlab.com/mgr8/realworld/db/sqlc"
)

type Server struct {
	Router gin.Engine
	Store  db.Queries
}

func NewServer(store db.Queries) *Server {
	server := &Server{Store: store}
	router := gin.Default()

	router.POST("/api/users", server.createUser)
	router.POST("/api/users/login", server.loginUser)

	server.Router = *router
	return server
}

func (server *Server) Start(address string) error {
	return server.Router.Run(address)
}

func erroresponse(err error) gin.H {
	return gin.H{"errors": err.Error()}
}
