package api

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	gonanoid "github.com/matoous/go-nanoid/v2"
	db "gitlab.com/mgr8/realworld/db/sqlc"
	"golang.org/x/crypto/bcrypt"
)

type userRequest struct {
	Username string `json:"username" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type createUserRequest struct {
	User userRequest `json:"user" binding:"required"`
}

func (server *Server) createUser(ctx *gin.Context) {
	var req createUserRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, erroresponse(err))
		return
	}

	id, err := gonanoid.New(6)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(req.User.Password), SALT)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}
	arg := db.CreateUserParams{
		Username: req.User.Username,
		Email:    req.User.Email,
		Password: string(pass),
		ID:       id,
		Bio:      sql.NullString{},
		Token:    sql.NullString{},
		Image:    sql.NullString{},
	}

	user, err := server.Store.CreateUser(ctx, arg)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}
	u, err := server.generateJWT(ctx, user.Username, user.Email)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}

	ctx.JSON(http.StatusOK, u)
}

type loginRequest struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type loginUserRequest struct {
	User loginRequest `json:"user" binding:"required"`
}

func (server *Server) loginUser(ctx *gin.Context) {
	var req loginUserRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, erroresponse(err))
		return
	}

	user, err := server.Store.GetUser(ctx, req.User.Email)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.User.Password))

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}
	u, err := server.generateJWT(ctx, user.Username, user.Email)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, erroresponse(err))
		return
	}
	ctx.JSON(http.StatusOK, u)

}
