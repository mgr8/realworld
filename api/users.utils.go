package api

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	db "gitlab.com/mgr8/realworld/db/sqlc"
	"time"
)

const SALT = 8
const jwtKey = "my-signing-key"
const jwtIssuer = "realworld"

func showUser(user db.GetUserRow) gin.H {
	return gin.H{
		"email":    user.Email,
		"username": user.Username,
		"bio":      user.Bio.String,
		"token":    user.Token.String,
		"image":    user.Image.String,
	}
}

type MyCustomClaims struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	jwt.StandardClaims
}

func (server *Server) generateJWT(ctx *gin.Context, username, email string) (gin.H, error) {
	u, err := server.Store.GetUser(ctx, email)
	if err != nil {
		return gin.H{}, err
	}
	if u.Token.Valid {
		oldToken, err := jwt.ParseWithClaims(u.Token.String, &MyCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtKey), nil
		})

		if err != nil {
			v, _ := err.(*jwt.ValidationError)
			if v.Errors != jwt.ValidationErrorExpired {
				return gin.H{}, err
			}
		}
		if _, ok := oldToken.Claims.(*MyCustomClaims); ok && oldToken.Valid {
			return gin.H{"user": showUser(u)}, nil
		}
	}
	claims := MyCustomClaims{
		Username: username,
		Email:    email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 12).Unix(),
			Issuer:    jwtIssuer,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedLocal, err := token.SignedString([]byte(jwtKey))
	args := db.UpdateTokenParams{
		Email: email,
		Token: sql.NullString{
			String: signedLocal,
			Valid:  true,
		},
	}
	err = server.Store.UpdateToken(ctx, args)
	if err != nil {
		return gin.H{}, err
	}
	s, err := server.Store.GetUser(ctx, email)
	if err != nil {
		return gin.H{}, err
	}
	return gin.H{"user": showUser(s)}, nil
}
