-- name: CreateUser :one
INSERT INTO "users" (
	username, email, password, id, bio, token, image
) VALUES (
	$1, $2, $3, $4, $5, $6, $7
)
RETURNING username, email, token, bio, image;


-- name: GetUser :one
SELECT username, email, token, bio, image, password FROM users WHERE email = $1;

-- name: UpdateToken :exec
UPDATE users SET token = $2 WHERE email = $1;