package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/mgr8/realworld/api"
	db "gitlab.com/mgr8/realworld/db/sqlc"
	"gitlab.com/mgr8/realworld/utils"
)

func main() {
	c := utils.GetConfig()
	fmt.Println(c.GetDBString())
	conn, err := sql.Open("postgres", c.GetDBString())
	if err != nil {
		log.Fatal("Cannot Connect to database")
	}

	store := db.New(conn)
	server := api.NewServer(*store)

	err = server.Start(c.GetAddrString())
	if err != nil {
		log.Fatal(err)
	}
}
