package utils

import (
	"fmt"

	"github.com/spf13/viper"
)

type Config struct {
	APP_PORT int
	APP_HOST string

	DB_NAME string
	DB_USER string
	DB_PASS string
	DB_HOST string
	DB_PORT int
}

func GetConfig() Config {
	var C Config

	viper.SetConfigFile(".env")

	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	err := viper.Unmarshal(&C)
	if err != nil {
		panic(err)
	}

	return C
}

func (config *Config) GetDBString() string {
	// return "postgresql://root:secret@localhost:5432/sample_bank?sslmode=disable"
	return fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", config.DB_USER, config.DB_PASS, config.DB_HOST, config.DB_PORT, config.DB_NAME)
}

func (config *Config) GetAddrString() string {
	return fmt.Sprintf("%s:%d", config.APP_HOST, config.APP_PORT)
}
